//
//  ViewController.swift
//  DTT_yolov8n
//
//  Created by Maruslam Darama on 7/2/2567 BE.

import UIKit
import AVFoundation
import Vision

class ViewController: CaptureViewController {
    
    @IBOutlet weak private var previewView: UIView!
    private var requests = [VNRecognizedObjectObservation]()
    
    let classificationResult = UILabel.init(frame: CGRect.init(x: 20, y: 20, width: UIScreen.main.bounds.width - 40, height: 40))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        super.setupAVCature(previewView)
        
        setupClassifier()
        setupView()
        startCaptureSession()
        
    }

    func getClassifierRequestResults(_ results: [Any]) {
        guard let observations = results as? [VNRecognizedObjectObservation] else {
            return
        }
        
        // Filter ตัวที่มีความมั่นใจสูงสุด
        let maxConfidenceObservation = observations.max { $0.confidence < $1.confidence }
        
        guard let objectObservation = maxConfidenceObservation else {
            return
        }
        
        // ตรวจสอบการแสดงข้อความบน Console
        print("Label: \(objectObservation.labels.first?.identifier ?? "")")
        print("Confidence: \(objectObservation.confidence)")
        
        // กำหนด label ตาม object ที่ตรวจพบ
        self.classificationResult.text = objectObservation.confidence > 0.96 ? "I am \(String(format: "%.3f", (objectObservation.confidence * 100)))% sure this is a \(objectObservation.labels.first?.identifier ?? "")." : "--"
        
        // ตรวจสอบการแสดงข้อความบน Console
        print("Classification Result Text: \(String(describing: self.classificationResult.text))")
    }
    
    func setupView() {
        self.view.addSubview(self.classificationResult)
    }
    
    func setupClassifier() {
        guard let modelURL = Bundle.main.url(forResource: "yolov8n", withExtension: "mlpackage") else {
            return
        }
        do {
            let model = try! yolov8n(contentsOf: modelURL)  // ให้ YOLOv8n เป็นชื่อคลาสที่ทำการ generate จาก ml package
            let visionModel = try VNCoreMLModel(for: model.model)
            classificationResult.text = ""
            let objectRecognition = VNCoreMLRequest(model: visionModel, completionHandler: { (request, error) in DispatchQueue.main.async(execute: {
                if let results = request.results {
                    self.getClassifierRequestResults(results)
                }
            })
            })
            objectRecognition.imageCropAndScaleOption = .scaleFit
            self.requests = [VNRecognizedObjectObservation]()
        } catch let error as NSError {
            print("Model loading went wrong: \(error)")
        }
    }

    
    override func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }

        let reqOrientation = reqOrientationfromDeviceOrientation()

        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer, orientation: reqOrientation, options: [:])
        do {
            try imageRequestHandler.perform(self.requests)
        } catch {
            print("Error in captureOutput: \(error)")
        }
    }

}
